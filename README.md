# README #

RobocodeStarterKit stores events in an event list to allow you sort and prioritise actions.

### Setup ###

* Download Robocode https://sourceforge.net/projects/robocode/files/
* Download Eclipse Neon http://www.eclipse.org/neon/
* Follow the setup guide robowiki.net/wiki/Robocode/Eclipse

### Questions ###

* make@muddygames.com