/*package cw;

import robocode.*;
import java.util.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;

public class CW208StarterKit extends Robot {

	// Holds list of current events
	ArrayList<Event> events = new ArrayList<Event>();

	// Define Turn Direction
	enum TURN_DIRECTION {
		NONE, RIGHT, LEFT
	};

	// Random number for varied movement
	Random random = new Random();
	int max = 3;
	int min = 1;

	// Robot should be in Reconnaissance or in a Action
	enum STATE {
		RECONNAISSANCE, ACTION, SCAN
	};

	STATE state = STATE.RECONNAISSANCE;

	// Current turn direction
	TURN_DIRECTION current_turn_direction = TURN_DIRECTION.RIGHT;
	// Last turn direction
	TURN_DIRECTION last_turn_direction = TURN_DIRECTION.NONE;

	// Define behaviour_robot_current of Robot
	enum BEHAVIOUR {
		UNDEFINED, NONE, DODGE, RAM, FLEE, FIRE, TRACK, WALL
	};

	BEHAVIOUR behaviour_robot_current = BEHAVIOUR.NONE; // Should move
	BEHAVIOUR behaviour_robot_last = BEHAVIOUR.NONE; // Should move

	double rotate_robot_amount = 0.0; // Rotate Amount

	double enemy_energy_last = 0.0; // Last energy of Enemy
	double enemy_energy_current = 0.0; // Current Energy of Enemy
	double enemy_distance = 0.0; // Distance to the Enemy
	double enemy_energy = 0.0; // Enemy Energy
	double absolute_bearing_to_enemy = 0.0; // Bearing to Enemy
	double enemy_bearing_from_gun = 0.0; // Bearing from Gun

	double turn_robot_amount = 0.0; // Basic turn gun amount
	double move_robot_amount = 50; // Robot basic movement amount
	double turn_gun_amount = 5.0; // Basic turn gun amount
	double gun_fire_power = 0.0; // Firepower

	public void run() {
		// ahead(800);
		turnGunRight(turn_gun_amount);
		while (true) {
			if (state == STATE.RECONNAISSANCE) {
				reconnaissance();
			} else if (state == STATE.ACTION) {
				issue_orders();
			} else {
				state = STATE.RECONNAISSANCE;
			}
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		events.add(e);
	}

	public void onHitWall(HitWallEvent e) {
		events.add(e);
	}

	public void onHitByBullet(HitByBulletEvent e) {
		events.add(e);
	}

	public void reset_all_values(){
		rotate_robot_amount = 0.0;

		enemy_energy_last = 0.0;
		enemy_energy_current = 0.0;
		enemy_distance = 0.0;
		enemy_energy = 0.0;
		absolute_bearing_to_enemy = 0.0;
		enemy_bearing_from_gun = 0.0;

		turn_robot_amount = 0.0;
		move_robot_amount = 50;
		turn_gun_amount = 5.0;
		gun_fire_power = 0.0;

	}

	public void issue_orders() {

		switch (behaviour_robot_current) {
		case FLEE:
			if (current_turn_direction == TURN_DIRECTION.NONE
					|| last_turn_direction == TURN_DIRECTION.LEFT) {
				turnRight(rotate_robot_amount);
			} else {
				turnLeft(rotate_robot_amount);
			}
			last_turn_direction = current_turn_direction;

			// Vary the moment distance
			move_robot_amount = move_robot_amount
					* (-(random.nextInt(max + 1 - min) + min));
			ahead(move_robot_amount);

			behaviour_robot_last = behaviour_robot_current;
			behaviour_robot_current = BEHAVIOUR.NONE;
			break;
		case WALL:
			if (current_turn_direction == TURN_DIRECTION.NONE
					|| last_turn_direction == TURN_DIRECTION.LEFT) {
				turnRight(rotate_robot_amount);
			} else {
				turnLeft(rotate_robot_amount);
			}
			last_turn_direction = current_turn_direction;
			// Vary the moment distance
			move_robot_amount = move_robot_amount - 1;
			ahead(move_robot_amount);

			behaviour_robot_last = behaviour_robot_current;
			behaviour_robot_current = BEHAVIOUR.NONE;
			break;
		case FIRE:
			fire(3);
			move_robot_amount = move_robot_amount - 1;
			ahead(move_robot_amount);

			behaviour_robot_last = behaviour_robot_current;
			behaviour_robot_current = BEHAVIOUR.NONE;
			break;

		default:
			scan();
		}

		// Based on events take action
		turnGunRight(turn_gun_amount);

		reset_all_values();

		// Do Recon Again
		state = STATE.RECONNAISSANCE;
	}

	public void reconnaissance() {
		for (Event e : events) {
			if (e instanceof robocode.HitByBulletEvent) {
				System.out.println("Hit by Bullet");
				rotate_robot_amount = normalRelativeAngleDegrees(90 - (getHeading() - ((robocode.HitByBulletEvent) e)
						.getHeading()));
				behaviour_robot_current = BEHAVIOUR.FLEE;
				break;
			}

			else if (e instanceof robocode.HitWallEvent) {
				System.out.println("Hit Wall ");
				rotate_robot_amount = normalRelativeAngleDegrees(90 - (getHeading() - ((robocode.HitWallEvent) e)
						.getBearing()));
				behaviour_robot_current = BEHAVIOUR.WALL;
				break;
			}

			else if (e instanceof robocode.HitRobotEvent) {
				enemy_energy = ((robocode.HitRobotEvent) e).getEnergy();
				System.out.println("Hit Robot @ Energy : " + enemy_energy);
				turn_robot_amount = normalRelativeAngleDegrees(((robocode.HitRobotEvent) e)
						.getBearing() + getHeading() - getGunHeading());
				behaviour_robot_current = BEHAVIOUR.RAM;
				break;
			}

			else if (e instanceof robocode.ScannedRobotEvent) {
				System.out.println("Scanned Robot ");
				absolute_bearing_to_enemy = getHeading()
						+ ((robocode.ScannedRobotEvent) e).getBearing();
				enemy_bearing_from_gun = normalRelativeAngleDegrees(absolute_bearing_to_enemy
						- getGunHeading());

				behaviour_robot_current = BEHAVIOUR.FIRE;
				break;
			}
		}

		events.clear(); // Clear events list and make decisions next round

		// Complete Action
		state = STATE.ACTION;

		// Calculate exact location of the robot

		
		 * If the event list contains a hit wall then turn away from the wall
		 * clear the event list If the energy of the enemy has just dropped they
		 * may have fired do a side step clear the the event list if the event
		 * contains a Scanned Robot Event Based on the distance fire a round of
		 * suitable power clear the event list
		 
	}
}
*/